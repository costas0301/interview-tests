package bowlingscore;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import bowlingscore.data.BowlingFrame;

public class BowlingScoreCalculatorTests {

	/**
	 * https://en.wikipedia.org/wiki/Ten-pin_bowling
	 * Scoring
	 */
	@Test
	public void strike() {
		BowlingScoreCalculator scoreCalculator = new BowlingScoreCalculatorImpl();
		List<BowlingFrame> frames = scoreCalculator.rollAndScore(new int[] {10, 3, 6});
		Assert.assertEquals(19, frames.get(0).getScore());
		Assert.assertEquals(28, frames.get(1).getScore());
		Assert.assertEquals(2, frames.size());
	}

	@Test
	public void doublePinfall() {
		BowlingScoreCalculator scoreCalculator = new BowlingScoreCalculatorImpl();
		List<BowlingFrame> frames = scoreCalculator.rollAndScore(new int[] {10, 10, 9, 0});
		Assert.assertEquals(29, frames.get(0).getScore());
		Assert.assertEquals(48, frames.get(1).getScore());
		Assert.assertEquals(57, frames.get(2).getScore());
		Assert.assertEquals(3, frames.size());
	}

	@Test
	public void turkeyPinfall() {
		BowlingScoreCalculator scoreCalculator = new BowlingScoreCalculatorImpl();
		List<BowlingFrame> frames = scoreCalculator.rollAndScore(new int[] {10, 10, 10, 8, 2, 8, 0});
		Assert.assertEquals(30, frames.get(0).getScore());
		Assert.assertEquals(58, frames.get(1).getScore());
		Assert.assertEquals(78, frames.get(2).getScore());
		Assert.assertEquals(96, frames.get(3).getScore());
		Assert.assertEquals(104, frames.get(4).getScore());
		Assert.assertEquals(5, frames.size());
	}

	@Test
	public void multipleStrikes() {
		BowlingScoreCalculator scoreCalculator = new BowlingScoreCalculatorImpl();
		List<BowlingFrame> frames = scoreCalculator.rollAndScore(new int[] {10, 10, 10, 10, 10, 7, 2});
		Assert.assertEquals(30, frames.get(0).getScore());
		Assert.assertEquals(60, frames.get(1).getScore());
		Assert.assertEquals(90, frames.get(2).getScore());
		Assert.assertEquals(117, frames.get(3).getScore());
		Assert.assertEquals(136, frames.get(4).getScore());
		Assert.assertEquals(145, frames.get(5).getScore());
		Assert.assertEquals(6, frames.size());
	}

	@Test
	public void spare() {
		BowlingScoreCalculator scoreCalculator = new BowlingScoreCalculatorImpl();
		List<BowlingFrame> frames = scoreCalculator.rollAndScore(new int[] {7, 3, 4, 2});
		Assert.assertEquals(14, frames.get(0).getScore());
		Assert.assertEquals(20, frames.get(1).getScore());
		Assert.assertEquals(2, frames.size());
	}

	/**
	 * Assignment „Bowling Game“
	 * Scoring
	 */
	@Test
	public void typicalGameExample() {
		BowlingScoreCalculator scoreCalculator = new BowlingScoreCalculatorImpl();
		List<BowlingFrame> frames = scoreCalculator.rollAndScore(new int[] {1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 10, 2, 8, 6});
		Assert.assertEquals(5, frames.get(0).getScore());
		Assert.assertEquals(14, frames.get(1).getScore());
		Assert.assertEquals(29, frames.get(2).getScore());
		Assert.assertEquals(49, frames.get(3).getScore());
		Assert.assertEquals(60, frames.get(4).getScore());
		Assert.assertEquals(61, frames.get(5).getScore());
		Assert.assertEquals(77, frames.get(6).getScore());
		Assert.assertEquals(97, frames.get(7).getScore());
		Assert.assertEquals(117, frames.get(8).getScore());
		Assert.assertEquals(133, frames.get(9).getScore());
		Assert.assertEquals(2, frames.get(9).getFirstChancePinFall());
		Assert.assertEquals(8, frames.get(9).getLastChancePinFall());
		Assert.assertEquals(6, frames.get(9).getAdditionalChancePinFall().intValue());
		Assert.assertEquals(10, frames.size());
	}

	@Test
	public void maxScoreExample() {
		BowlingScoreCalculator scoreCalculator = new BowlingScoreCalculatorImpl();
		List<BowlingFrame> frames = scoreCalculator.rollAndScore(new int[] {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10});
		Assert.assertEquals(30, frames.get(0).getScore());
		Assert.assertEquals(60, frames.get(1).getScore());
		Assert.assertEquals(90, frames.get(2).getScore());
		Assert.assertEquals(120, frames.get(3).getScore());
		Assert.assertEquals(150, frames.get(4).getScore());
		Assert.assertEquals(180, frames.get(5).getScore());
		Assert.assertEquals(210, frames.get(6).getScore());
		Assert.assertEquals(240, frames.get(7).getScore());
		Assert.assertEquals(270, frames.get(8).getScore());
		Assert.assertEquals(300, frames.get(9).getScore());
		Assert.assertEquals(10, frames.get(9).getFirstChancePinFall());
		Assert.assertEquals(10, frames.get(9).getLastChancePinFall());
		Assert.assertEquals(10, frames.get(9).getAdditionalChancePinFall().intValue());
		Assert.assertEquals(10, frames.size());
	}


}
