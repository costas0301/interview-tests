package bowlingscore;

import java.util.ArrayList;
import java.util.List;

import bowlingscore.data.BowlingFrame;

public class BowlingScoreCalculatorImpl implements BowlingScoreCalculator {

	public static final int MAX_FRAMES = 10;

	@Override
	public List<BowlingFrame> rollAndScore(int[] pinsDown) {
		List<BowlingFrame> frames = new ArrayList<>(MAX_FRAMES);
		BowlingFrame currentFrame = null;
		for (int i = 0; i < pinsDown.length; i++) {
			int pf = pinsDown[i];
			if (pf > 10) {
				throw new IllegalArgumentException("It's impossible hit more than 10 pins");
			}
			if (isInGame(frames)) {
				if (currentFrame == null || currentFrame.isFrameClosed()) {
					currentFrame = new BowlingFrame(pf, getCurrentScore(frames));
					frames.add(currentFrame);
					if (currentFrame.isStrike()) {
						incrementScore4Strike(currentFrame, pinsDown, i);
						currentFrame = null;
					}
					continue;
				}
				currentFrame.setLastChancePinFallWithScore(pf);
				if (currentFrame.isSpare()) {
					incrementScore4Spare(currentFrame, pinsDown, i);
				}
				currentFrame = null;
			} else {
				BowlingFrame lastFrame = getLastFrame(frames);
				int nextValue = i;
				if (lastFrame.isStrike()) {
					lastFrame.setLastChancePinFall(pf);
					nextValue ++;
				}
				if ((lastFrame.isSpare() || lastFrame.isStrike()) && nextValue < pinsDown.length) {
					lastFrame.setAdditionalChancePinFall(pinsDown[nextValue]);
				}
				break;
			}
		}
		return frames;
	}

	private int getCurrentScore(List<BowlingFrame> frames) {
		return frames.isEmpty() ? 0 : getLastFrame(frames).getScore();
	}

	private BowlingFrame getLastFrame(List<BowlingFrame> frames) {
		return frames.get(frames.size() - 1);
	}

	private boolean isInGame(List<BowlingFrame> frames) {
		return frames.size() < MAX_FRAMES || !getLastFrame(frames).isFrameClosed();
	}

	private void incrementScore4Strike(BowlingFrame bowlingFrame, int[] pinsDown, int pinFallIndex) {
		incrementScore(bowlingFrame, pinsDown, pinFallIndex, 2);
	}

	private void incrementScore4Spare(BowlingFrame bowlingFrame, int[] pinsDown, int pinFallIndex) {
		incrementScore(bowlingFrame, pinsDown, pinFallIndex, 1);
	}

	private void incrementScore(BowlingFrame bowlingFrame, int[] pinsDown, int pinFallIndex, int amount) {
		for (int i = 1; i <= amount; i++) {
			if (pinFallIndex + i < pinsDown.length) {
				bowlingFrame.incrementScore(pinsDown[pinFallIndex + i]);
			}
		}
	}

	void validate() {
		// if (firstChancePinFall + secondChancePinFall > 10) {
		// throw new IllegalArgumentException("It's impossible more than 10 pins
		// to fall");
		// }
		// if (lastChancePinFall > 0) {
		// throw new IllegalArgumentException("It's impossible to throw more
		// than two times");
		// }

	}
}
