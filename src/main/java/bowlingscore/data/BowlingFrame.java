package bowlingscore.data;

public final class BowlingFrame {

	public final int FRAME_MAX_PINFALLS = 10;

	private int firstChancePinFall;
	private int lastChancePinFall;
	private Integer additionalChancePinFall;
	private boolean frameClosed;
	private int score = 0;

	public BowlingFrame(int firstChancePinFall, int score) {
		this.firstChancePinFall = firstChancePinFall;
		if (isStrike()) {
			frameClosed = true;
		}
		this.score = score + firstChancePinFall;
	}

	@Override
	public String toString() {
		return String.format("firstChancePinFall=%d; lastChancePinFall=%d; score=%d, frameClosed=%b", firstChancePinFall, lastChancePinFall, score, frameClosed);
	}

	public void incrementScore(int score) {
		this.score += score;
	}

	public boolean isStrike() {
		return firstChancePinFall == FRAME_MAX_PINFALLS;
	}

	public int getFramePinFalls() {
		return firstChancePinFall + lastChancePinFall;
	}

	public boolean isSpare() {
		return getFramePinFalls() == FRAME_MAX_PINFALLS;
	}

	public void setLastChancePinFallWithScore(int lastChancePinFall) {
		this.lastChancePinFall = lastChancePinFall;
		frameClosed = true;
		this.score += lastChancePinFall;
	}

	/* getters & setters */
	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public boolean isFrameClosed() {
		return frameClosed;
	}

	public int getFirstChancePinFall() {
		return firstChancePinFall;
	}

	public int getLastChancePinFall() {
		return lastChancePinFall;
	}

	public void setLastChancePinFall(int lastChancePinFall) {
		this.lastChancePinFall = lastChancePinFall;
	}

	public Integer getAdditionalChancePinFall() {
		return additionalChancePinFall;
	}

	public void setAdditionalChancePinFall(Integer additionalChancePinFall) {
		this.additionalChancePinFall = additionalChancePinFall;
	}

}
