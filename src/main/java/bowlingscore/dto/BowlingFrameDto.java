package bowlingscore.dto;

import bowlingscore.data.BowlingFrame;

public class BowlingFrameDto {

	public final int firstChancePinFall;
	public final int lastChancePinFall;
	public final Integer additionalChancePinFall;
	public final boolean frameClosed;
	public final int score;
	public final boolean strike;
	public final boolean spare;

	public BowlingFrameDto(BowlingFrame frame) {
		this.firstChancePinFall = frame.getFirstChancePinFall();
		this.lastChancePinFall = frame.getLastChancePinFall();
		this.frameClosed = frame.isFrameClosed();
		this.additionalChancePinFall = frame.getAdditionalChancePinFall();
		this.strike = frame.isStrike();
		this.spare = frame.isSpare();
		this.score = frame.getScore();
	}
}
