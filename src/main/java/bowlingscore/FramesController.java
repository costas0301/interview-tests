package bowlingscore;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import bowlingscore.data.BowlingFrame;
import bowlingscore.dto.BowlingFrameDto;

@Controller
public class FramesController {

	@SuppressWarnings("serial")
	@RequestMapping(value = "/rest/frames", method = RequestMethod.POST)
	@ResponseBody
	public String frames(@RequestBody String body) {
		BowlingScoreCalculator calculator = new BowlingScoreCalculatorImpl();

		Gson gson = new Gson();
		List<Integer> data = gson.fromJson(body, new TypeToken<List<Integer>>() {}.getType());
		List<BowlingFrame> frames = calculator.rollAndScore(data.stream().mapToInt(i -> i).toArray());
		List<BowlingFrameDto> framesDto = frames.stream().map(frame -> new BowlingFrameDto(frame)).collect(Collectors.toList());
		return gson.toJson(framesDto);
	}

}
