package bowlingscore;

import java.util.List;

import bowlingscore.data.BowlingFrame;

public interface BowlingScoreCalculator {

	/**
	 *
	 * @param pinsDown - how many pins were knock down by the ball
	 * @return current game score
	 */
	List<BowlingFrame> rollAndScore(int[] pinsDown);
}
